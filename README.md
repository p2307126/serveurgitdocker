# Get started
This repository builds 3 docker containers, one server and two clients. The first initializes a git repository, while the other two can connect to the server automatically (without any prompt).

## Prerequisites
Our work has been done with the official version of Docker. As a result, we cannot guarantee compatibility with versions of Docker available from distro maintainers' repositories, due to version differences.

### Uninstall other versions
We suggest you install the official version of Docker with the following commands:

>This will remove your existing Docker installation :
```bash
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove -y $pkg; done
```

### Install using the apt repository
>1. Set up Docker's apt repository
```bash
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

>2. Install the latest version of Docker packages.
```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
>3. Verify that the Docker Engine installation is successful by running the hello-world image.
```bash
sudo docker run hello-world
```
You have now successfully installed and started Docker Engine.

## Clone the repository
Use `mkdir` to create a directory, then `cd` to go in it. Finally use the `git clone` command to repatriate the repository.
```bash
git clone https://forge.univ-lyon1.fr/p2307126/serveurgitdocker
```
You are now ready to use docker with our repository !

## Enjoy our repository
First of all, make sure you are at the root of the repository.

>1. Docker operations require administrator rights, so :

```bash
su -i
```
Please use your administrator password here.

>2. Just use this command tu start the building of containers
```bash
docker compose up
```

This command will use the docker-compose.yml file to create the 3 containers.

## How it works ?

The docker-compose.yml will create 3 containers, git-server, git-alice and git-bob using their respective Dockerfile, found in /git-server/git, /git-client/alice and /git-client/bob.

**git-server**, the container acting as server will use a Dockerfile to build a generic ubuntu image, installing the `openssh-server` and `git` packages and creating a git user. The file also calls a start_ssh.sh script, which will force the ssh service to start when the container is started, and create a git repository in which a file containing text will be added.

**git-alice** and **git-bob**, the client containers, are identical. Their respective Dockerfile will install the `openssh-client`, `sshpass` and `git` packages. The only difference between these files is the user created. alice for git-alice, and bob for git-bob.

The docker-compose.yml file will then tell the two clients to add git-server (ip address 192.168.1.101) to the list of known servers, before generating an ssh key and sending it to the git user.

#### WARNING
***In this step, the password of the git user (git) is sent in clear text during copying, to avoid any interaction during manipulation. For security reasons, once the keys have been copied, it would be best practice to prevent git-server from accepting ssh connections to git using its password, which is no longer necessary, as the keys have been copied to git.***

